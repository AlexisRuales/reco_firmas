
import os
import sys
import json
import datetime
import numpy as np
import skimage.draw


ROOT_DIR = os.getcwd()
if ROOT_DIR.endswith("peso"):
    
    ROOT_DIR = os.path.dirname(os.path.dirname(ROOT_DIR))


sys.path.append(ROOT_DIR)
from configuracion import Config
import utils
import modelo as modellib

COCO_WEIGHTS_PATH = os.path.join(ROOT_DIR, "mask_rcnn_coco.h5")

DEFAULT_LOGS_DIR = os.path.join(ROOT_DIR, "logs")

############################################################
#  Configurations
############################################################


class FirmaConfig(Config):

    NAME = "firma"


    IMAGES_PER_GPU = 2


    NUM_CLASSES = 1 + 1  
    
    STEPS_PER_EPOCH = 100

    
    DETECTION_MIN_CONFIDENCE = 0.9


############################################################
#  Dataset
############################################################

class FirmaDataset(utils.Dataset):

    def load_firma(self, dataset_dir, subset):

        self.add_class("firma", 1, "firma")

        
        assert subset in ["train", "val"]
        dataset_dir = os.path.join(dataset_dir, subset)

        
        # VGG Image Annotator saves each image in the form:
        # { 'filename': '28503151_5b5b7ec140_b.jpg',
        #   'regions': {
        #       '0': {
        #           'region_attributes': {},
        #           'shape_attributes': {
        #               'all_points_x': [...],
        #               'all_points_y': [...],
        #               'name': 'polygon'}},
        #       ... more regions ...
        #   },
        #   'size': 100202
        # }
        
        annotations = json.load(open(os.path.join(dataset_dir, "via_region_data.json")))
        annotations = list(annotations.values())  # don't need the dict keys


        annotations = [a for a in annotations if a['regions']]

        
        for a in annotations:

            polygons = [r['shape_attributes'] for r in a['regions'].values()]

            image_path = os.path.join(dataset_dir, a['filename'])
            image = skimage.io.imread(image_path)
            height, width = image.shape[:2]

            self.add_image(
                "firma",
                image_id=a['firma'],  
                path=image_path,
                width=width, height=height,
                polygons=polygons)

    def load_mask(self, image_id):

        image_info = self.image_info[image_id]
        if image_info["source"] != "firma":
            return super(self.__class__, self).load_mask(image_id)


        info = self.image_info[image_id]
        mask = np.zeros([info["height"], info["width"], len(info["polygons"])],
                        dtype=np.uint8)
        for i, p in enumerate(info["polygons"]):
           
            rr, cc = skimage.draw.polygon(p['all_points_y'], p['all_points_x'])
            mask[rr, cc, i] = 1

       
        return mask, np.ones([mask.shape[-1]], dtype=np.int32)

    def image_reference(self, image_id):
      
        info = self.image_info[image_id]
        if info["source"] == "firma":
            return info["path"]
        else:
            super(self.__class__, self).image_reference(image_id)


def train(model):

    dataset_train = FirmaDataset()
    dataset_train.load_firma(args.dataset, "train")
    dataset_train.prepare()


    dataset_val = FirmaDataset()
    dataset_val.load_firma(args.dataset, "val")
    dataset_val.prepare()


    print("Training network heads")
    model.train(dataset_train, dataset_val,
                learning_rate=config.LEARNING_RATE,
                epochs=30,
                layers='heads')


def color_splash(image, mask):

    gray = skimage.color.gray2rgb(skimage.color.rgb2gray(image)) * 255
    
    mask = (np.sum(mask, -1, keepdims=True) >= 1)

    if mask.shape[0] > 0:
        splash = np.where(mask, image, gray).astype(np.uint8)
    else:
        splash = gray
    return splash


def detect_and_color_splash(model, image_path=None, video_path=None):
    assert image_path or video_path


    if image_path:
        
        print("Running on {}".format(args.image))
   
        image = skimage.io.imread(args.image)
   
        r = model.detect([image], verbose=1)[0]
   
        splash = color_splash(image, r['masks'])
        
        file_name = "splash_{:%Y%m%dT%H%M%S}.png".format(datetime.datetime.now())
        skimage.io.imsave(file_name, splash)
    elif video_path:
        import cv2
     
        vcapture = cv2.VideoCapture(video_path)
        width = int(vcapture.get(cv2.CAP_PROP_FRAME_WIDTH))
        height = int(vcapture.get(cv2.CAP_PROP_FRAME_HEIGHT))
        fps = vcapture.get(cv2.CAP_PROP_FPS)

   
        file_name = "splash_{:%Y%m%dT%H%M%S}.avi".format(datetime.datetime.now())
        vwriter = cv2.VideoWriter(file_name,
                                  cv2.VideoWriter_fourcc(*'MJPG'),
                                  fps, (width, height))

        count = 0
        success = True
        while success:
            print("frame: ", count)
         
            success, image = vcapture.read()
            if success:
               
                image = image[..., ::-1]
                
                r = model.detect([image], verbose=0)[0]
                
                splash = color_splash(image, r['masks'])
                
                splash = splash[..., ::-1]
               
                vwriter.write(splash)
                count += 1
        vwriter.release()
    print("Saved to ", file_name)


############################################################
#  Training
############################################################

if __name__ == '__main__':
    import argparse


    parser = argparse.ArgumentParser(
        description='Train Mask R-CNN to detect firma.')
    parser.add_argument("command",
                        metavar="<command>",
                        help="'train' or 'splash'")
    parser.add_argument('--dataset', required=False,
                        metavar="/path/to/firma/dataset/",
                        help='Directory of the Firma dataset')
    parser.add_argument('--weights', required=True,
                        metavar="/path/to/weights.h5",
                        help="Path to weights .h5 file or 'coco'")
    parser.add_argument('--logs', required=False,
                        default=DEFAULT_LOGS_DIR,
                        metavar="/path/to/logs/",
                        help='Logs and checkpoints directory (default=logs/)')
    parser.add_argument('--image', required=False,
                        metavar="path or URL to image",
                        help='Image to apply the color splash effect on')
    parser.add_argument('--video', required=False,
                        metavar="path or URL to video",
                        help='Video to apply the color splash effect on')
    args = parser.parse_args()


    if args.command == "train":
        assert args.dataset, "Argument --dataset is required for training"
    elif args.command == "splash":
        assert args.image or args.video,\
               "Provide --image or --video to apply color splash"

    print("Weights: ", args.weights)
    print("Dataset: ", args.dataset)
    print("Logs: ", args.logs)

    if args.command == "train":
        config = FirmaConfig()
    else:
        class InferenceConfig(FirmaConfig):
            
            GPU_COUNT = 1
            IMAGES_PER_GPU = 1
        config = InferenceConfig()
    config.display()


    if args.command == "train":
        model = modellib.MaskRCNN(mode="training", config=config,
                                  model_dir=args.logs)
    else:
        model = modellib.MaskRCNN(mode="inference", config=config,
                                  model_dir=args.logs)

    
    if args.weights.lower() == "coco":
        weights_path = COCO_WEIGHTS_PATH
        
        if not os.path.exists(weights_path):
            utils.download_trained_weights(weights_path)
    elif args.weights.lower() == "last":
        
        weights_path = model.find_last()[1]
    elif args.weights.lower() == "imagenet":
       
        weights_path = model.get_imagenet_weights()
    else:
        weights_path = args.weights


    print("Loading weights ", weights_path)
    if args.weights.lower() == "coco":

        model.load_weights(weights_path, by_name=True, exclude=[
            "mrcnn_class_logits", "mrcnn_bbox_fc",
            "mrcnn_bbox", "mrcnn_mask"])
    else:
        model.load_weights(weights_path, by_name=True)

 
    if args.command == "train":
        train(model)
    elif args.command == "splash":
        detect_and_color_splash(model, image_path=args.image,
                                video_path=args.video)
    else:
        print("'{}' is not recognized. "
              "Use 'train' or 'splash'".format(args.command))
